//  credit to http://detectmobilebrowsers.com/
window.mobileCheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

$( document ).ready( function () {

	$( '#TOS-toggle' ).on( "click", function() {
		$( '#TOS' ).slideToggle();
	});

	$( '#read-TOS' ).on( "click", function() {
		//  this checks if the checkbox is checked. If it is, enable the input, otherwise disable it.
		if ( $( this ).prop( 'checked' ) ) {
			$( "#connect" ).prop( "disabled", false );
		} else {
			$( "#connect" ).prop( "disabled", true );
		}
	});

	//  the slideshow
	if( !window.mobileCheck() ) {
		var i = 3;
		window.setInterval( function() {
			$( '.background.next' ).fadeIn( 600 );
			$( '.background.prev2' ).remove();
			$( '.background.prev' ).addClass( 'prev2' );
			$( '.background.next' ).addClass( 'prev' );
			$( '.backgruond.prev' ).removeClass( 'next' );

			elem = '<div class="background next" style="background-image: url(images/bg/' + i + '.jpg); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></div>';
			$( '#background-wrapper' ).append( elem );

			if ( i++ >= 5 ) {
				i = 1;
			}
		}, 5000 );
	}

	//  this keeps the voucher input properly formatted
	$( '#voucher' ).on( 'keypress', function( e ) {
		if ( e.which >= 48 && e.which <= 57 ) {
			value = $( this ).val();

			if ( value.length == 5 ) {
				$( this ).val( value + '-' );
			} else if ( value.length >= 11 ) {
				e.preventDefault();
			}
		/*
		Let me tell you a story.

		Once upon a time, I was a naieve young web-developer who thought you could use a input with a type="number" to
		input a number with dashes in it. This is not the case, as dashes are not allowed by the number input type, and
		besides, that wouldn't have worked in any IE less than 10.

		"I'll just write some input-checking to change the input to a number-and-dash only", I said to myself.

		30 minutes later, after wrestling with regexes and char codes, I finally settled upon narrowing the input down
		by its char code in a range, ignoring all inputs that didn't lie between 48 and 57 (numbers), and letting
		anything less than 33 pass (control characters).

		Ha.

		Apparently, despite every reference I could find pointing to the fact that the dash was a char code 189, it
		seemed to continue popping up and not being blocked. It even got inputted when the input was already 11
		characters long. So, the only option was that it was less than a char code 33. And I would have made an
		exception for it, if there wasn't also the issue of the majority of non-alphanumeric characters also passing the
		char code check.

		So, I went back on the character code method and ran a regex on String.fromCharCode( e.which ). And, it seemed
		to work. Everything was going okay, I was getting expected behavior... Until the Firefox test. Apparently, in
		Chrom(e|ium), you get an empty character out when you fromCharCode a backspace, which made it through my regex.
		You know what you get in Firefox?

		␈.

		Needless to say, this did not pass my regex for numbers or dashes, and thus, backspace, enter, arrow keys, and
		all other control characters did not work in this input.

		What did I do, you ask?

		I made a regex. A regex that should never have been born. A regex that checks the literal ASCII code of the
		char code of the input event, and outrules all control characters.

		And it works. Firefox, IE, the works.
		 */
	 } else if ( /[\x20-\x7E]/.test( String.fromCharCode( e.which ) ) ) {
			e.preventDefault();
		}
	});

});
